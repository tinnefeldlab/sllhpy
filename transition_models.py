import os
import ctypes
from numpy.ctypeslib import ndpointer
import numpy as np
os.add_dll_directory(r"C:\Users\Lennart Grabenhorst\LRZ Sync+Share\sandbox\c_mlh")
os.add_dll_directory(os.getcwd())

#cFile = ctypes.CDLL("libchess.so")
two_states_single_lib = ctypes.CDLL("./dlls/two_states-single.so", winmode=0) # we somehow have to do this in order for ctypes to find the DLLs.

two_states_single_lib = ctypes.cdll.LoadLibrary("./dlls/two_states-single.so")
fun2 = two_states_single_lib.llh
fun2.restype = ctypes.c_double
fun2.argtypes = [ctypes.c_double*4,
                ndpointer(dtype=np.int_,ndim=1,flags="C_CONTIGUOUS"),
                ndpointer(dtype=np.double,ndim=1,flags="C_CONTIGUOUS"),
                ctypes.c_int]
params2 = (ctypes.c_double*4)()
nop2 = (ctypes.c_int)()

three_states_single_lib = ctypes.cdll.LoadLibrary("./dlls/three_states-single.so")
fun3 = three_states_single_lib.llh
fun3.restype = ctypes.c_double
fun3.argtypes = [ctypes.c_double*5,
                ndpointer(dtype=np.int_,ndim=1,flags="C_CONTIGUOUS"),
                ndpointer(dtype=np.double,ndim=1,flags="C_CONTIGUOUS"),
                ctypes.c_int]
params3 = (ctypes.c_double*5)()
nop3 = (ctypes.c_int)()

three_states_single_ei_lib = ctypes.cdll.LoadLibrary("./dlls/three_states-single-ei.so")
fun4 = three_states_single_ei_lib.llh
fun4.restype = ctypes.c_double
fun4.argtypes = [ctypes.c_double*6,
                ndpointer(dtype=np.int_,ndim=1,flags="C_CONTIGUOUS"),
                ndpointer(dtype=np.double,ndim=1,flags="C_CONTIGUOUS"),
                ctypes.c_int]
params4 = (ctypes.c_double*6)()
nop4 = (ctypes.c_int)()


three_states_blinking = ctypes.cdll.LoadLibrary("./dlls/three_states_blinking.so")
fun6 = three_states_blinking.llh
fun6.restype = ctypes.c_double
fun6.argtypes = [ctypes.c_double*8,
                ndpointer(dtype=np.int_,ndim=1,flags="C_CONTIGUOUS"),
                ndpointer(dtype=np.double,ndim=1,flags="C_CONTIGUOUS"),
                ctypes.c_int]
params6 = (ctypes.c_double*8)()
nop6 = (ctypes.c_int)()

two_states_blinking = ctypes.cdll.LoadLibrary("./dlls/two_states_blinking.so")
fun7 = two_states_blinking.llh
fun7.restype = ctypes.c_double
fun7.argtypes = [ctypes.c_double*6,
                ndpointer(dtype=np.int_,ndim=1,flags="C_CONTIGUOUS"),
                ndpointer(dtype=np.double,ndim=1,flags="C_CONTIGUOUS"),
                ctypes.c_int]
params7 = (ctypes.c_double*6)()
nop7 = (ctypes.c_int)()


def two_states_single_llh(param,ids,tips):
    nop2.value = len(ids)
    params2[0],params2[1],params2[2],params2[3] = param[0],param[1],param[2],param[3]
    return fun2(params2,ids,tips,nop2)

def three_states_single_llh(param,ids,tips):
    nop3.value = len(ids)
    params3[0],params3[1],params3[2],params3[3],params3[4] = param[0],param[1],param[2],param[3],param[4]
    return fun3(params3,ids,tips,nop3)

def three_states_single_ei_llh(param,ids,tips):
    nop4.value = len(ids)
    params4[0],params4[1],params4[2],params4[3],params4[4],params4[5] = param[0],param[1],param[2],param[3],param[4],param[5]
    return fun4(params4,ids,tips,nop4)

def three_states_blinking_llh(param,ids,tips):
    nop6.value = len(ids)
    params6[0],params6[1],params6[2],params6[3],params6[4],params6[5],params6[6],params6[7] = param[0],param[1],param[2],param[3],param[4],param[5],param[6],param[7]
    return fun6(params6,ids,tips,nop6)

def two_states_blinking_llh(param,ids,tips):
    nop7.value = len(ids)
    params7[0],params7[1],params7[2],params7[3],params7[4],params7[5] = param[0],param[1],param[2],param[3],param[4],param[5]
    return fun7(params7,ids,tips,nop7)
