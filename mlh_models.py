class Two_states_single:
    '''Two state model (instantaneous transition)
      as defined in Chungs Nature 2013
      Faster implementation with Ctypes'''
    def __init__(self,photonstream):
        import numpy as np
        self.det, self.tip = photonstream[0].astype(np.int_),photonstream[1].astype(np.double)
        
    def llh(self,params):  ## Likelihood for whole trace (vini = 1, 1, 1; vfin = peq)
        from transition_models import two_states_single_llh
        return two_states_single_llh(params,self.det,self.tip)

class Three_states_single:
    ## Remember that kon and koff for three state have to be 2* kon and koff of the two state model
    '''Two state model (instantaneous transition)
      as defined in Chungs Nature 2013
      Faster implementation with Ctypes'''
    def __init__(self,photonstream):
        import numpy as np
        self.det, self.tip = photonstream[0].astype(np.int_),photonstream[1].astype(np.double)
        
    def llh(self,params):  ## Likelihood for whole trace (vini = 1, 1, 1; vfin = peq)
        from transition_models import three_states_single_llh
        return three_states_single_llh(params,self.det,self.tip)

class Three_states_single_ei:
    ## Remember that kon and koff for three state have to be 2* kon and koff of the two state model
    '''Two state model (instantaneous transition)
      as defined in Chungs Nature 2013
      Faster implementation with Ctypes'''
    def __init__(self,photonstream):
        import numpy as np
        self.det, self.tip = photonstream[0].astype(np.int_),photonstream[1].astype(np.double)
        
    def llh(self,params):  ## Likelihood for whole trace (vini = 1, 1, 1; vfin = peq)
        from transition_models import three_states_single_ei_llh
        return three_states_single_ei_llh(params,self.det,self.tip)


class Three_states_blinking:
    ## Remember that kon and koff for three state have to be 2* kon and koff of the two state model
    '''Two state model (instantaneous transition)
      as defined in Chungs Nature 2013
      Faster implementation with Ctypes'''
    def __init__(self,photonstream):
        import numpy as np
        self.det, self.tip = photonstream[0].astype(np.int_),photonstream[1].astype(np.double)
        
    def llh(self,params):  ## Likelihood for whole trace (vini = 1, 1, 1; vfin = peq)
        from transition_models import three_states_blinking_llh
        return three_states_blinking_llh(params,self.det,self.tip)

class Two_states_blinking:
    ## Remember that kon and koff for three state have to be 2* kon and koff of the two state model
    '''Two state model (instantaneous transition)
      as defined in Chungs Nature 2013
      Faster implementation with Ctypes'''
    def __init__(self,photonstream):
        import numpy as np
        self.det, self.tip = photonstream[0].astype(np.int_),photonstream[1].astype(np.double)
        
    def llh(self,params):  ## Likelihood for whole trace (vini = 1, 1, 1; vfin = peq)
        from transition_models import two_states_blinking_llh
        return two_states_blinking_llh(params,self.det,self.tip)
