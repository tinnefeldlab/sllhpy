import sys,os
from pathlib import PurePath
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../')
import numpy as np 
import matplotlib.pyplot as plt
plt.ion()
import art
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

print(bcolors.HEADER+bcolors.BOLD+art.text2art("sllhpy v1.0",font="small")+bcolors.ENDC)
print(bcolors.WARNING+"Lennart Grabenhorst, 12/2023"+bcolors.ENDC)


class transitions:

    def __init__(self,folder):
        """
        Load folder of traces with transitions stored in files exported via the export_folder or timetrace.export_transitions commands.
        If folder == False, initialise an empty class (for simulating transitions via transitions.simulate_transitions())
        For simulations, photoblinking is currently *not* implemented. 
        """
        self.tracewise_results=[]
        if folder:
            import alive_progress
            self.directory = PurePath(folder)
            self.name = PurePath(folder).stem
            self.binding_transitions, self.high_int_means = {},{}
            self.keys = []
            import h5py as h5
            with alive_progress.alive_bar(len(os.listdir(folder)),dual_line=True) as bar:
                for i in os.listdir(folder):
                    file = h5.File(folder+"/"+i,"r")
                    for key in file["binding"].keys():
                        self.binding_transitions[i+"_"+key] = np.array(file["binding"][key])
                        self.high_int_means[i+"_"+key] = file.attrs["high_int_means"]
                        self.keys.append(i+"_"+key)
                    bar.text = f"Loaded transitions of trace {i}. Total: {len(self.binding_transitions)} binding events."
                    file.close()
                    bar()
        pass

    def hist(self,key,binning=1e-5, type="pr"):
        """
        Plot a histogram of the transition of key "key". 
        """
        ids,tips = self.binding_transitions[key]
        timestamps = np.cumsum(tips)
        donor, acceptor = timestamps[ids==0], timestamps[ids==1]
        bins = np.arange(0,np.max(timestamps),binning)
        dh, ah = np.histogram(donor,bins)[0], np.histogram(acceptor,bins)[0]
        if type:
            fr = ah/(dh+ah)
        if np.any(self.tracewise_results):
            plt.title(f"{key}, {self.tracewise_results[key][0]*1e6:.02f} µs")
            ei = self.tracewise_results[key][1]
            eu_tw = self.e_transition_wise[key][0]
            ef_tw = self.e_transition_wise[key][1]
            eu_trace = self.high_int_means[key][0]
            ef_trace = self.high_int_means[key][1]
            ei_tw = (ef_tw-eu_tw)*ei+eu_tw
            ei_trace = (ef_trace-eu_trace)*ei+eu_trace
            plt.axhline(ei_tw,linestyle="--",color="tab:red")
            plt.axhline(ei_trace,linestyle="--",color="tab:red")
        if np.any(self.e_transition_wise):
            plt.axhline(self.e_transition_wise[key][0],color="tab:orange",label="Transition wise")
            plt.axhline(self.e_transition_wise[key][1],color="tab:pink",label="Transition wise")

        plt.axhline(self.high_int_means[key][0],color="tab:green",label="Trace wise")
        plt.axhline(self.high_int_means[key][1],color="tab:red",label="Trace wise")    
        plt.legend()
        plt.step(bins[1:],fr)
        
    
    def transition_wise_e(self,options={"xatol":1e-8}):
        """
        Calculate Eu, Ef, k, pb for each transition separately using two-state model with blinking. 
        After calling this, self.e_transition_wise is available which is a dict
        with dict[key][0] being Eu and dict[key][1] being Ef and dict[key][2] being k and dict[key][3] being pb.
        """
        import alive_progress
        import mlh_models as mlh
        from scipy.optimize import minimize
        self.e_transition_wise = {}

        with alive_progress.alive_bar(len(self.keys),dual_line=True) as bar:

            for key in self.keys:
                lo_estimate,hi_estimate = self.high_int_means[key][0],self.high_int_means[key][1]
                transition = self.binding_transitions[key]
                s2 = mlh.Two_states_blinking(transition)
                def func(params):
                    return -s2.llh([params[0],params[1],0.01,params[2]*1e7,0.01,params[3]])#changed to 0.1 04.09.23
                res = minimize(func,x0=[hi_estimate,lo_estimate,0.1,0.9],bounds=((1e-9,1-1e-9),(1e-9,1-1e-9),(0,1),(0.2,1-1e-3)),options=options)["x"]
                res[2] = res[2]*1e7
                self.e_transition_wise[key] = res
                bar.text = f"Fitting transition {key}.. Last result: {res[0]:.03f},{res[1]:.03f},{res[2]:.03f},{res[3]:.03f} "
                bar()

    def transition_wise_e_minuit(self,options={"xatol":1e-8}):
        """
        Calculate Eu, Ef, k, pb for each transition separately using two-state model with blinking. 
        After calling this, self.e_transition_wise is available which is a dict
        with dict[key][0] being Eu and dict[key][1] being Ef and dict[key][2] being k and dict[key][3] being pb.
        This function uses iMinuit as minimization algorithm.
        """
        import alive_progress
        import mlh_models as mlh
        from iminuit import Minuit
        self.e_transition_wise = {}

        with alive_progress.alive_bar(len(self.keys),dual_line=True) as bar:

            for key in self.keys:
                lo_estimate,hi_estimate = self.high_int_means[key][0],self.high_int_means[key][1]
                transition = self.binding_transitions[key]
                kapp_estimate = 0.01 #1/(np.sum(transition[1])/2)
                s2 = mlh.Two_states_blinking(transition)
                def func(hi,lo,k,pb):
                    return -s2.llh([hi,lo,0.01,k,kapp_estimate,pb])
                m = Minuit(func,hi=hi_estimate,lo=lo_estimate,k=1e6,pb=0.9)
                m.print_level = 0
                m.limits = [(0,1),(0,1),(1e2,1e8),(0,1)]
                m.migrad()
                res = m.values
                self.e_transition_wise[key] = res
                bar.text = f"Fitting transition {key}.. Last result: {res[0]:.03f},{res[1]:.03f},{res[2]:.03f},{res[3]:.03f} "
                bar()
    def likelihood_ei(self,key,tpt,ei,fitted_e=False,scale_e=True,hardcode_e=np.empty(0)):
        """
        Calculate Δ ln L for transition "key" with parameters tpt and ei for τi and ei. 
        if fitted_e == True, this uses the transition wise results obtained via self.transition_wise_e, otherwise this uses tracewise values obtained via HMM.
        if scale_e == True this scales the ei value to eu and ef, otherwise uses raw FRET efficiency values.
        if hardcode_e == True, uses hardcoded values for eu and ef given in the array hardcode_e.
        """
        import mlh_models as mlh
        transition = self.binding_transitions[key]
        s2 = mlh.Two_states_blinking(transition)
        s3 = mlh.Three_states_blinking(transition)
        k = self.e_transition_wise[key][2]
        p_b = self.e_transition_wise[key][3]
        if fitted_e:          
            lo = self.e_transition_wise[key][1]
            hi = self.e_transition_wise[key][0]
        elif np.any(hardcode_e):
            lo = hardcode_e[0]
            hi = hardcode_e[1]
        else:
            lo = self.high_int_means[key][0]
            hi = self.high_int_means[key][1]
        if scale_e==True:
                ei_scaled = (hi-lo)*ei+lo
                if ei_scaled>1:
                    ei_scaled=1
        if scale_e==False: 
                ei_scaled = ei
        
        base = s2.llh([hi,lo,0.01,k,0.01,p_b]) 
        result = s3.llh([hi,ei_scaled,lo,0.02,k,1/(2*tpt),0.02,p_b])-base
        if np.isnan(base):
            print("NAN!!")
        if np.isnan(result):
                print("Nan in s3!!")
        return result

    def sum_of_likelihoods_ei(self,tpt,ei,in_keys=np.empty(0),fitted_e=False,scale_e=True,hardcode_e=np.empty(0)):
        """
        Calculate Δ ln L for all transitions with parameters tpt and ei for τi and ei. 
        if fitted_e == True, this uses the transition wise results obtained via self.transition_wise_e, otherwise this uses tracewise values obtained via HMM.
        if scale_e == True this scales the ei value to eu and ef, otherwise uses raw FRET efficiency values.
        if hardcode_e == True, uses hardcoded values for eu and ef given in the array hardcode_e.
        if in_keys uses indices provided in the array in_keys, otherwise uses all transitions.
        
        """
        if in_keys.any():
            transition_keys = np.array(self.keys)[in_keys]
        else:
            transition_keys = np.array(self.keys)
        result=0
        for index, key in enumerate(transition_keys):
            result+= self.likelihood_ei(key,tpt,ei,fitted_e=fitted_e,scale_e=scale_e, hardcode_e=hardcode_e)
        return result
    
    
    def most_likely_tpt_ei(self,tracewise=False,tol=1e-7,start_values=[2e-5,0.5],bounds_ei=[0,5],bounds_tpt=[1e-9,1e-4],in_keys=np.empty(0),fitted_e=False,scale_e=True,hardcode_e=np.empty(0),method="Nelder-Mead",options={"xatol":1e-7}):
        """
        Calculate most likely parameters tpt and ei (τi and ei) for all transitions.
        if in_keys: uses indices provided in the array in_keys, otherwise uses all transitions.

        
        """ 
        import alive_progress
        from scipy.optimize import basinhopping,minimize
        def func(params):
            return -self.sum_of_likelihoods_ei(params[0],params[1],in_keys=in_keys,fitted_e=fitted_e,scale_e=scale_e,hardcode_e=hardcode_e)
        def func2(params,key):
            return -self.likelihood_ei(key,params[0],params[1],fitted_e=fitted_e,scale_e=scale_e,hardcode_e=hardcode_e)
        if tracewise:
            self.tracewise_results={}
            if in_keys.any():
                transitions = np.array(self.keys)[in_keys]
            else:
                transitions = np.array(self.keys)
            result = np.zeros((len(transitions),2))
            lengths = np.zeros(len(transitions))
            with alive_progress.alive_bar(len(transitions), dual_line=True) as bar:
                for ix, key in enumerate(transitions):
                    result[ix] = basinhopping(lambda params: func2(params,key),niter=30, x0=[2e-5,1.1], minimizer_kwargs={"method":method,"options":options,"bounds":((bounds_tpt[0],bounds_tpt[1]),(bounds_ei[0],bounds_ei[1]))})["x"]
                    lengths[ix] = np.sum(self.binding_transitions[key][1])
                    bar.text = f"-> fitting transition {key} ({ix+1}/{len(transitions)}). Last result: tpt:{result[ix][0]*1e6:.02f} µs, ei:{result[ix][1]:.02f} ({lengths[ix]*1e6:.02f} µs region) .."
                    bar()
                    self.tracewise_results[key] = result[ix]
            return result,lengths
        else: 
            self.scipy_likelihood_res = minimize(func,x0=start_values,bounds=((bounds_tpt[0],bounds_tpt[1]),(bounds_ei[0],bounds_ei[1])),method=method,options=options)
            return self.scipy_likelihood_res["x"]

    def most_likely_tpt_ei_minuit(self,scale_e=True,fitted_e=True, in_keys=np.empty(0), hardcode_e=np.empty(0)):
        """
        Calculate most likely parameters tpt and ei (τi and ei) for all transitions.
        if in_keys: uses indices provided in the array in_keys, otherwise uses all transitions.
        Uses iMinuit algorithm for the minimization.
        
        """ 
        from iminuit import Minuit
        def func(tpt,ei):
            return -self.sum_of_likelihoods_ei(tpt,ei,in_keys=in_keys,fitted_e=fitted_e,scale_e=scale_e,hardcode_e=hardcode_e)
        
        m = Minuit(func,tpt=2e-5,ei=0.5)
        m.print_level = 0
        m.errordef = Minuit.LIKELIHOOD
        m.limits = [(1e-9,1e-3),(0,1)]
        m.migrad()
        self.migrad_likelihood_res = m
        print(self.migrad_likelihood_res.values)
    
    def bootstrap_tpt_ei_minuit(self,nsamples=1000,scale_e=True,fitted_e=True, in_keys=np.empty(0), hardcode_e=np.empty(0)):
        """
        Calculate most likely parameters tpt and ei (τi and ei) for all transitions.
        if in_keys: uses indices provided in the array in_keys, otherwise uses all transitions.
        Uses iMinuit algorithm for the minimization.
        In this case: bootstrapping over nsamples
        
        """ 
        self.migrad_likelihood_res_bootstrap = []
        if in_keys.any():
            transition_keys = in_keys
        else:
            transition_keys = range(len(self.keys))
        import alive_progress
        from iminuit import Minuit
        

        with alive_progress.alive_bar(nsamples,dual_line=True) as bar:
            for i in range(nsamples):
                sample = np.random.choice(transition_keys,size=len(transition_keys))
                def func2(tpt,ei):
                    return -self.sum_of_likelihoods_ei(tpt,ei,in_keys=sample,fitted_e=fitted_e,scale_e=scale_e,hardcode_e=hardcode_e)
                m = Minuit(func2,tpt=2e-5,ei=0.5)
                m.print_level = 0
                m.errordef = Minuit.LIKELIHOOD
                m.limits = [(1e-9,1e-3),(0,1)]
                m.migrad()
                self.migrad_likelihood_res_bootstrap.append(m.values)
                bar.text = f"-> fitting bootstrap {i+1}/{nsamples}, last tpt: {m.values[0]*1e6:.02f} µs, last ei: {m.values[1]:.02f}"
                bar()
    
        

    def contour_data(self,ei_bins=np.linspace(0,1,20),tpt_bins=np.logspace(-7,-3,20),in_keys=np.empty(0),fitted_e=False,scale_e=True,hardcode_e=np.empty(0)):
        '''
        Calculate a 2d contour plot with tpt vs ei. 
        in_keys can be used to only use a subset of the data
        fitted_e whether to use the fitted e values per transition or the tracewise values
        scale_e whether to scale to eu = 0 and ef = 1
        hardcode_e input your own e values (lo=[0], hi=[1])
        '''
        import alive_progress
        r=np.zeros((len(ei_bins),len(tpt_bins)))
        with alive_progress.alive_bar(len(ei_bins),dual_line=True) as bar:
            for ix, i in enumerate(ei_bins):
                for jx, j in enumerate(tpt_bins):
                    
                    r[ix,jx] = self.sum_of_likelihoods_ei(j,i,in_keys,fitted_e=fitted_e,scale_e=scale_e,hardcode_e=hardcode_e)
                bar.text = f"-> calculating contour data in ranges ei:{np.min(ei_bins)}-{np.max(ei_bins)} tpt:{np.min(tpt_bins)}-{np.max(tpt_bins)}"
                bar()
        return r,ei_bins,tpt_bins

    def single_likelihoods_ei(self,tpt,ei,scale_e=True,fitted_e=False,in_keys=np.empty(0),hardcode_e=np.empty(0)):
        if in_keys.size:
            transitions = np.array(self.keys)[in_keys]
        else:
            transitions = np.array(self.keys)
        result=0
        result = np.zeros((len(transitions)))
        for ix, key in enumerate(transitions):
            result[ix] = self.likelihood_ei(key,tpt,ei,fitted_e=fitted_e,scale_e=scale_e,hardcode_e=hardcode_e)
        return result

    def mean_countrates(self):
        self.countrates = {}
        for key in self.keys:
            transition = self.binding_transitions[key][1]
            length = np.sum(transition)
            number_of_photons = len(transition)
            countrate = number_of_photons/length
            self.countrates[key] = countrate
        return np.array(self.countrates.values())

    def plot_all_transitions(self,directory,binning=1e-4):
        plt.ioff()
        means = np.array([i for i in self.high_int_means.values()])
        if self.e_transition_wise:
            means = np.array([i for i in self.e_transition_wise.values()])
        for key in self.keys:
            ids,tips = self.binding_transitions[key]
            timestamps = np.cumsum(tips)
            donor, acceptor = timestamps[ids==0], timestamps[ids==1]
            bins = np.arange(0,np.max(timestamps),binning)
            donor_h, acceptor_h = np.histogram(donor,bins)[0], np.histogram(acceptor,bins)[0]
            f_h = acceptor_h/(acceptor_h+donor_h)
            ax,(f1,f2) = plt.subplots(2,1,sharex=True,figsize=(8,4))
            f1.step(bins[1:],donor_h)
            f1.step(bins[1:],acceptor_h)
            f2.step(bins[1:],f_h)
            f2.axhline(means[:,1].mean(),color="red",linestyle="--")
            f2.axhline(means[:,0].mean(),color="red",linestyle="--")
            f2.set_xlabel("Time / s")
            f1.set_ylabel(f"Counts / {binning*1e3} ms")
            f2.set_ylabel("PR")
            plt.savefig(directory+key+".png")
            plt.clf()