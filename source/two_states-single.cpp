#include <iostream>
#include <Eigen/Dense>
#include <math.h>

 // compile with g++ -I C:/eigen-3.4.0 -Ofast -shared -o test.so .\three_states_blinking_low_parameters.cpp
 // i mixed up kf and ku in this file, but it does not matter since they are set to the same value anyway in the analysis.

using Eigen::Matrix2d;
using Eigen::Vector2d;

Matrix2d acc(double ef, double eu, double kf, double ku, double tau) {
    Matrix2d mat;
    mat(0,0) = ef*(kf*exp(tau*(kf + ku)) + ku)*exp(-tau*(kf + ku))/(kf + ku);
    mat(0,1) = ef*kf*(exp(tau*(kf + ku)) - 1)*exp(-tau*(kf + ku))/(kf + ku);
    mat(1,0) = eu*ku*(exp(tau*(kf + ku)) - 1)*exp(-tau*(kf + ku))/(kf + ku);
    mat(1,1) = eu*(kf + ku*exp(tau*(kf + ku)))*exp(-tau*(kf + ku))/(kf + ku);
    return mat;
}
Matrix2d don(double ef, double eu, double kf, double ku, double tau) {
    Matrix2d mat;
    mat(0,0) = -(ef - 1)*(kf*exp(tau*(kf + ku)) + ku)*exp(-tau*(kf + ku))/(kf + ku);
    mat(0,1) = kf*(1 - exp(tau*(kf + ku)))*(ef - 1)*exp(-tau*(kf + ku))/(kf + ku);
    mat(1,0) = ku*(1 - exp(tau*(kf + ku)))*(eu - 1)*exp(-tau*(kf + ku))/(kf + ku);
    mat(1,1) = -(eu - 1)*(kf + ku*exp(tau*(kf + ku)))*exp(-tau*(kf + ku))/(kf + ku);
    return mat;
}
Vector2d init_acc(double eu) {
    Vector2d vec;
    vec(0) = 0;
    vec(1) = eu;
    return vec;
}
Vector2d init_don(double eu) {
    Vector2d vec;
    vec(0) = 0;
    vec(1) = 1-eu;
    return vec;
}

double two_states_llh(double pars[4], int* ids, double* tips, int number_of_photons){
  double ef = pars[0];
  double eu = pars[1];
  double kf = pars[2];
  double ku = pars[3];
  double alpha_sum=0;
  double alpha;
  Vector2d initial;
  Matrix2d matrix;
  if(ids[0]==1){ // Acceptor photon first
    initial = init_acc(eu);
  }
  if(ids[0]==0){
    initial = init_don(eu);
  }
  for(int value = 1; value!= number_of_photons; ++value){
    if(ids[value]==1){
      matrix = acc(ef,eu,kf,ku,tips[value]);    
      initial = matrix*initial;
      alpha = 1/(initial(0)+initial(1));
      initial = initial*alpha;   
      alpha_sum = alpha_sum+log(alpha);   
    }
    else if(ids[value]==0){
      matrix = don(ef,eu,kf,ku,tips[value]);
      initial = matrix*initial;
      alpha = 1/(initial(0)+initial(1));
      initial = initial*alpha;
      alpha_sum = alpha_sum+log(alpha);
    }            
      
  }
  double result = log(initial(0))-alpha_sum;
  return result;
}

extern "C" double llh(double pars[4], int* ids, double* tips, int number_of_photons)
{
    double result = two_states_llh(pars, ids, tips, number_of_photons);
    return result;
} 

int main() {
    return 0;
}