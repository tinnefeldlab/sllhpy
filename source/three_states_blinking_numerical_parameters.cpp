#include <unsupported/Eigen/MatrixFunctions>
#include <iostream>
#include <Eigen/Dense>
#include <math.h>
 
 // compile with g++ -I C:/eigen-3.4.0 -Ofast -shared -o test.so .\three_states_blinking_low_parameters.cpp
 
using Eigen::MatrixXd;
using Eigen::VectorXd;

MatrixXd K(double k_D, double k, double k_TC, double k_Aapp, double p_b) {
    MatrixXd mat(6,6);
    mat(0,0) = -k_D-(k*(1-p_b));
    mat(0,1) = k_TC;
    mat(0,2) = 0;
    mat(0,3) = p_b*k;
    mat(0,4) = 0;
    mat(0,5) = 0;
    mat(1,0) = k_D;
    mat(1,1) = -2*k_TC-((1-p_b)*k);
    mat(1,2) = k_Aapp;
    mat(1,3) = 0;
    mat(1,4) = k*p_b;
    mat(1,5) = 0;
    mat(2,0) = 0;
    mat(2,1) = k_TC;
    mat(2,2) = -k_Aapp-((1-p_b)*k);
    mat(2,3) = 0;
    mat(2,4) = 0;
    mat(2,5) = k*p_b;
    mat(3,0) = (1-p_b)*k;
    mat(3,1) = 0;
    mat(3,2) = 0;
    mat(3,3) = -k_D-(k*p_b);
    mat(3,4) = k_TC;
    mat(3,5) = 0;
    mat(4,0) = 0;
    mat(4,1) = (1-p_b)*k;
    mat(4,2) = 0;
    mat(4,3) = k_D;
    mat(4,4) = -2*k_TC-(p_b*k);
    mat(4,5) = k_Aapp;
    mat(5,0) = 0;
    mat(5,1) = 0;
    mat(5,2) = (1-p_b)*k;
    mat(5,3) = 0;
    mat(5,4) = k_TC;
    mat(5,5) = -k_Aapp-(p_b*k);
    return mat;
}

MatrixXd F_acc(double E_B, double E_I, double E_U) {
    MatrixXd mat(6,6);
    mat(0,0) = E_B;
    mat(0,1) = 0;
    mat(0,2) = 0;
    mat(0,3) = 0;
    mat(0,4) = 0;
    mat(0,5) = 0;
    mat(1,0) = 0;
    mat(1,1) = E_I;
    mat(1,2) = 0;
    mat(1,3) = 0;
    mat(1,4) = 0;
    mat(1,5) = 0;
    mat(2,0) = 0;
    mat(2,1) = 0;
    mat(2,2) = E_U;
    mat(2,3) = 0;
    mat(2,4) = 0;
    mat(2,5) = 0;
    mat(3,0) = 0;
    mat(3,1) = 0;
    mat(3,2) = 0;
    mat(3,3) = E_U;
    mat(3,4) = 0;
    mat(3,5) = 0;
    mat(4,0) = 0;
    mat(4,1) = 0;
    mat(4,2) = 0;
    mat(4,3) = 0;
    mat(4,4) = E_U;
    mat(4,5) = 0;
    mat(5,0) = 0;
    mat(5,1) = 0;
    mat(5,2) = 0;
    mat(5,3) = 0;
    mat(5,4) = 0;
    mat(5,5) = E_U;
    return mat;
}

MatrixXd F_don(double E_B, double E_I, double E_U) {
    MatrixXd mat(6,6);
    mat(0,0) = 1-E_B;
    mat(0,1) = 0;
    mat(0,2) = 0;
    mat(0,3) = 0;
    mat(0,4) = 0;
    mat(0,5) = 0;
    mat(1,0) = 0;
    mat(1,1) = 1-E_I;
    mat(1,2) = 0;
    mat(1,3) = 0;
    mat(1,4) = 0;
    mat(1,5) = 0;
    mat(2,0) = 0;
    mat(2,1) = 0;
    mat(2,2) = 1-E_U;
    mat(2,3) = 0;
    mat(2,4) = 0;
    mat(2,5) = 0;
    mat(3,0) = 0;
    mat(3,1) = 0;
    mat(3,2) = 0;
    mat(3,3) = 1-E_U;
    mat(3,4) = 0;
    mat(3,5) = 0;
    mat(4,0) = 0;
    mat(4,1) = 0;
    mat(4,2) = 0;
    mat(4,3) = 0;
    mat(4,4) = 1-E_U;
    mat(4,5) = 0;
    mat(5,0) = 0;
    mat(5,1) = 0;
    mat(5,2) = 0;
    mat(5,3) = 0;
    mat(5,4) = 0;
    mat(5,5) = 1-E_U;
    return mat;
}

VectorXd v_fin() {
    VectorXd vec(6);
    vec(0) = 1;
    vec(1) = 0;
    vec(2) = 0;
    vec(3) = 1;
    vec(4) = 0;
    vec(5) = 0;
    return vec;
}

VectorXd v_ini(double p_b) {
    VectorXd mat(6);
    mat(0) = 0;
    mat(1) = 0;
    mat(2) = p_b;
    mat(3) = 0;
    mat(4) = 0;
    mat(5) = 1-p_b;
    return mat;
}

double blinking_llh(double pars[8], int* ids, double* tips, int number_of_photons){
    double eb = pars[0];
    double ei = pars[1];
    double eu = pars[2];
    double k_D = pars[3];
    double k = pars[4];
    double k_TC = pars[5];
    double k_Aapp = pars[6];
    double p_b = pars[7];
    double alpha_sum = 0;
    double alpha;
    MatrixXd initial(6,6);
    MatrixXd matrix(6,6);
    MatrixXd Facc = F_acc(eb,ei,eu);
    MatrixXd Fdon = F_don(eb,ei,eu);
    VectorXd vini = v_ini(p_b);
    VectorXd vfin = v_fin();
    
    if(ids[0]==1){
        initial = Facc*vini;
    }
    if(ids[0]==0){
        initial = Fdon*vini;
    }
    for(int value = 1; value!= number_of_photons; ++value){
        if(ids[value]==1){
            matrix = K(k_D,k,k_TC,k_Aapp,p_b);
            matrix = matrix*tips[value];
            matrix = matrix.exp();
            matrix = Facc*matrix;
            initial = matrix*initial;
            alpha = 1/(initial(0)+initial(1)+initial(2)+initial(3)+initial(4)+initial(5));
            initial = initial*alpha;
            alpha_sum = alpha_sum+log(alpha);
        }
        else if(ids[value]==0){
            matrix = K(k_D,k,k_TC,k_Aapp,p_b);
            matrix = matrix*tips[value];
            matrix = matrix.exp();
            matrix = Fdon*matrix;
            initial = matrix*initial;
            alpha = 1/(initial(0)+initial(1)+initial(2)+initial(3)+initial(4)+initial(5));
            initial = initial*alpha;
            alpha_sum = alpha_sum+log(alpha);
        }
    }
    double result = log(initial(0)+initial(3))-alpha_sum;
    return result;
}

extern "C" double llh(double pars[8], int* ids, double* tips, int number_of_photons){
    double result = blinking_llh(pars,ids,tips,number_of_photons);
    return result;
}

int main(){
    /*
    MatrixXf mat = K(0.1,0.1,0.1,0.1,0.1);
    float tau = 0.2;
    MatrixXf mat2 = mat*tau;
    MatrixXf mat1 = v_ini();
    MatrixXf mat3 = F_acc(0.1,0.1,0.1);
    MatrixXf fd = mat3*mat1;
    std::cout << "The matrix E is:\n" << fd << "\n\n";
    std::cout << "The matrix A is:\n" << mat << "\n\n";
    std::cout << "The matrix exponential of A is:\n" << mat2.exp() << "\n\n";
*/
    double pars[8] = {0.4,0.2,0.1,0.1,0.1,0.1,0.1,0.1};
    int ids[240] = {1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0};
    double tips[240] = {0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002,0.01,0.004,0.01,0.004,0.01,0.004,0.01,0.004,0.009,0.002};
    int number_of_photons = 240;
    for(int value = 1; value!= 2000; ++value){
        double res = blinking_llh(pars,ids,tips,number_of_photons);
    }
/*
    std::cout << res;

    system("pause");
  */  
    return 0;
}
